package com.ajtech.bleservice.commands;

public enum FogMachineCommandType
{
    RequestActualTemperature("TempReq"),
    RequestAutoMode("ReqMode"),
    EnableAutoTemp("AutoTemp On"),
    DisableAutoTemp("AutoTemp Off"),
    SetTargetTemperature("SetReq"),
    TurnHeatOn("StartHeat"),
    TurnHeatOff("Stop");

    private String commandText;

    FogMachineCommandType(final String commandText) {
        this.commandText = commandText;
    }

    public String getCommandText() {
        return commandText;
    }
}
