package com.ajtech.bleservice.commands;

public abstract class BLECommand implements IBLECommand
{
    protected int intValue = -1;
    protected float floatValue = -1.0f;
    protected FogMachineCommandType commandType;

    @Override
    public BLEResponse execute() throws Exception {
        if(commandType == null) {
            throw new Exception("Command Type was null");
        }

        String bluetoothCommand = commandType.getCommandText();

        if(intValue >= 0) {
            bluetoothCommand += " " + intValue;
        }
        else if(floatValue >= 0.0f) {
            bluetoothCommand += " " + floatValue;
        }

        return new BLEResponse(bluetoothCommand);
    }

    public String getCommandText() {
        return commandType.getCommandText();
    }

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public void setFloatValue(float floatValue) {
        this.floatValue = floatValue;
    }

    public FogMachineCommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(FogMachineCommandType commandType) {
        this.commandType = commandType;
    }
}
