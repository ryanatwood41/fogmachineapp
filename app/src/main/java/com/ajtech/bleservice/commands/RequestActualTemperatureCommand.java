package com.ajtech.bleservice.commands;

public class RequestActualTemperatureCommand extends BLECommand
{
    public RequestActualTemperatureCommand() {
        this.commandType = FogMachineCommandType.RequestActualTemperature;
    }
}
