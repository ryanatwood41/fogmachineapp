package com.ajtech.bleservice.commands;

public class SetTargetTemperatureCommand extends BLECommand
{
    public SetTargetTemperatureCommand() {
        this.commandType = FogMachineCommandType.SetTargetTemperature;
    }
}
