package com.ajtech.bleservice.commands;

interface IBLECommand {
    BLEResponse execute() throws Exception;
}
