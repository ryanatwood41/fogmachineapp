package com.ajtech.bleservice;

import com.ajtech.bleservice.commands.BLECommand;
import com.ajtech.bleservice.commands.BLEResponse;

public abstract class BLEDeviceCommunicator implements IBLEDeviceCommunicator
{
    public BLEDeviceCommunicator() {
        mSerialManager = new SerialManager(this, this);
        setBleBaseDeviceManager(mSerialManager);
    }

    public BLEResponse sendCommand(BLECommand command) {
        return command.execute();
    }
}
