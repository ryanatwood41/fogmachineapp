package com.ajtech.fogmachine.communication;

import com.ajtech.bleservice.BLEDeviceCommunicator;
import com.ajtech.bleservice.commands.BLECommand;
import com.ajtech.bleservice.commands.BLEResponse;

import java.util.UUID;

public class FogMachineCommunicator extends BLEDeviceCommunicator
{
    private final static UUID UARTServiceUUID           = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
    private final static UUID UARTTxCharacteristicUUID  = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
    private final static UUID UARTRxCharacteristicUUID  = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");

    public FogMachineCommunicator() {
        super();
    }
}
