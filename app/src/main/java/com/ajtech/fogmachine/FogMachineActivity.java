package com.ajtech.fogmachine;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.ajtech.fogmachine.communication.FogMachineCommunicator;
import com.ajtech.fogmachine.ui.helper.AndroidUIHelper;

public class FogMachineActivity extends Activity
{
    boolean parse_error = false;
    private Context context;
    private final FogMachineCommunicator fogMachineCommunicator;

    public FogMachineActivity()
    {
        fogMachineCommunicator = new FogMachineCommunicator();
    }

    @SuppressLint("MissingSuperCall")
    protected void onCreate(final Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        context = getApplicationContext();

        MakeToast("Starting up...");

//        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            Log.v("Switch State=", ""+isChecked);
//
//            Button buttonScanButton = (Button) findViewById(R.id.btnScan);
//
//            if(isChecked)
//                buttonScanButton.setText("Daddy = Best");
//            else
//                buttonScanButton.setText("Daddy != Best");
//            }
//        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_fog_machine, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onResume() {
        super.onResume();
    }

    public void onUiVspServiceFound(boolean found) {

    }

    public void onUiSendDataSuccess(String dataSend) {

    }

    public void onUiReceiveData(final String dataReceived)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    AndroidUIHelper.Toast(context, "Received Data: " + dataReceived);
                }
                catch (NumberFormatException e)
                {
                    parse_error = true;
                    e.printStackTrace();
                }

                if(parse_error)
                {
                    parseError(dataReceived);
                }
            }
        });
    }

    private void parseError(String dataReceived)
    {
        parse_error = false;
        StringBuilder stringBuilder;
        stringBuilder = new StringBuilder(20);
        stringBuilder.append("Invalid Format: " + dataReceived);
        AndroidUIHelper.Toast(context, "Ryan");
    }

    public void onUiUploaded() {

    }

    public void onConnectionStateChange(final int newStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Do something
            }
        });
    }

    private void MakeToast(String toastText) {
        AndroidUIHelper.Toast(context, toastText);
    }
}