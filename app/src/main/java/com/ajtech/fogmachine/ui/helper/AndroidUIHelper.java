package com.ajtech.fogmachine.ui.helper;

import android.content.Context;
import android.widget.Toast;

public class AndroidUIHelper
{
    public static void ToastShort(Context context, CharSequence text)
    {
        Toast(context, text, Toast.LENGTH_SHORT);
    }

    public static void Toast(Context context, CharSequence text)
    {
        Toast(context, text, Toast.LENGTH_LONG);
    }

    public static void Toast(Context context, CharSequence text, int duration)
    {
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
